package com.finance.backend.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.UuidGenerator;

import java.util.Set;

@Entity
@Table(name = "categories")
public class CategoryEntity {

    @Id
    @GeneratedValue
    @UuidGenerator
    private String id;
    private String categoryName;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @OneToMany(mappedBy = "category")
    Set<EntryEntity> entries;

    public CategoryEntity() {}

    public CategoryEntity(String categoryName, UserEntity user) {
        this.categoryName = categoryName;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Set<EntryEntity> getEntries() {
        return entries;
    }

    public void setEntries(Set<EntryEntity> entries) {
        this.entries = entries;
    }

}
