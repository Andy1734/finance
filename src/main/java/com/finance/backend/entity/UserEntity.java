package com.finance.backend.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue
    @UuidGenerator
    private String id;
    private String firstName;
    private String lastName;
    private LocalDate joinedDate;
    private String email;
    private String password;
    @OneToMany(mappedBy = "user")
    private Set<CategoryEntity> categories;
    @OneToMany(mappedBy = "author")
    private Set<EntryEntity> entries;
    @OneToMany(mappedBy = "author")
    private Set<TemplateEntity> templates;

    public UserEntity() {}

    public UserEntity(String firstName, String lastName, LocalDate joinedDate, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.joinedDate = joinedDate;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(LocalDate joinedDate) {
        this.joinedDate = joinedDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<CategoryEntity> getCategories() {
        return categories;
    }

    public void setCategories(Set<CategoryEntity> categories) {
        this.categories = categories;
    }

    public Set<EntryEntity> getEntries() {
        return entries;
    }

    public void setEntries(Set<EntryEntity> entries) {
        this.entries = entries;
    }

    public Set<TemplateEntity> getTemplates() {
        return templates;
    }

    public void setTemplates(Set<TemplateEntity> templates) {
        this.templates = templates;
    }

}
