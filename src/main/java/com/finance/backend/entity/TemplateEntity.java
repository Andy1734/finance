package com.finance.backend.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.UuidGenerator;

@Entity
@Table(name = "templates")
public class TemplateEntity {

    @Id
    @GeneratedValue
    @UuidGenerator
    private String id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity author;

}
