package com.finance.backend.entity;

import com.finance.backend.util.EntryType;
import jakarta.persistence.*;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDate;

@Entity
@Table(name = "entries")
public class EntryEntity {

    @Id
    @GeneratedValue
    @UuidGenerator
    private String id;
    @Enumerated(EnumType.STRING)
    private EntryType entryType;
    private int amount;
    private LocalDate creationDate;
    @ManyToOne
    @JoinColumn(name = "author_id")
    private UserEntity author;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryEntity category;

    public EntryEntity() {}

    public EntryEntity(EntryType entryType, int amount, LocalDate creationDate, UserEntity author, CategoryEntity category) {
        this.entryType = entryType;
        this.amount = amount;
        this.creationDate = creationDate;
        this.author = author;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public EntryType getEntryType() {
        return entryType;
    }

    public void setEntryType(EntryType entryType) {
        this.entryType = entryType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public UserEntity getAuthor() {
        return author;
    }

    public void setAuthor(UserEntity author) {
        this.author = author;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

}
