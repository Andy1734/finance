package com.finance.backend.util;

public enum EntryType {
    EXPENSE,
    DEPOSIT
}
